package model.vo;
public class VOBusUpdate 
{
	private String VehicleNo;
	private long TripId;	
	private String RouteNo;
	private String Direction;
	private String Destination;
	private String Pattern;
	private double Latitude;
	private double Longitude;
	private String RecordTime;
	private String RouteMap;


	public VOBusUpdate(String pVehicleNo,long pTripId, String pRouteNo, String pDirection,String pDestination, String pPattern,double pLatitude,double pLongitude,String pRecordTime,String pRouteMap)
	{
		VehicleNo=pVehicleNo;
		TripId=pTripId;
		RouteNo=pRouteNo;
		Direction=pDirection;
		Destination=pDestination;
		Pattern=pPattern;
		Latitude=pLatitude;
		Longitude=pLongitude;
		RecordTime=pRecordTime;
		RouteMap=pRouteMap;
	}
	public String getVehicleNo()
	{
		return VehicleNo;
	}
	public long getTripId()
	{
		return TripId;
	}
	public String getRouteNo()
	{
		return RouteNo;
	}
	public String getdirection()
	{
		return Direction;
	}
	public String getDestination()
	{
		return Destination;
	}
	public String getPattern()
	{
		return Pattern;
	}
	public double getLatitude()
	{
		return Latitude;
	}
	public double getLongitude()
	{
		return Longitude;
	}
	public String getRecordTime()
	{
		return RecordTime;
	}

	public String getDirection() {
		return Direction;
	}

	public void setDirection(String direction) {
		this.Direction = direction;
	}

	public String getRouteMap() {
		return RouteMap;
	}


}

