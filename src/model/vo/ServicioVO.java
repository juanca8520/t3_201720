package model.vo;

import model.data_structures.Comparador;

public class ServicioVO implements Comparador<ServicioVO>
{

	//Atributos
	
	/**
	 * numero, tripId, routeNo, direccion, destination, pattern, latitude, longitude, time, routeMap
	 */
	
	private String numero;
	private long tripId;
	private String routeNo;
	private String direccion;
	private String destination;
	private String pattern;
	private double latitude;
	private double longitude;
	private String time;
	private String routeMap;
	
	public ServicioVO(String pNumero, long pTripId, String pRouteNo, String pDireccion, String pDestination, String pPattern, double pLatitude, double pLongitude, String pTime, String pRouteMap)
	{
		numero = pNumero;
		tripId = pTripId;
		routeNo = pRouteNo;
		direccion = pDireccion;
		destination = pDestination;
		pattern = pPattern;
		latitude = pLatitude;
		longitude = pLongitude;
		time = pTime;
		routeMap = pRouteMap;
		// TODO Auto-generated constructor stub
	}
	
	

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public long getTripId() {
		return tripId;
	}

	public void setTripId(long tripId) {
		this.tripId = tripId;
	}

	public String getRouteNo() {
		return routeNo;
	}

	public void setRouteNo(String routeNo) {
		this.routeNo = routeNo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getRouteMap() {
		return routeMap;
	}

	public void setRouteMap(String routeMap) {
		this.routeMap = routeMap;
	}

	@Override
	public int compareTo(ServicioVO arg1) {
		// TODO Auto-generated method stub
		return this.numero.hashCode() - arg1.numero.hashCode();

	}

	

}
