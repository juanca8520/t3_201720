package model.vo;

import model.data_structures.Comparador;

public class VOAgency implements Comparador<VOAgency>
{

	private String agencyId;
	private String agencyName;
	private String agencyUrl;
	private String agencyTimezone;
	private String agencyLang;
	
    public VOAgency(String pAgencyId, String pAgencyName, String pAgencyUrl,String pAgencyTimezone,String pAgencyLang)
    {
    	agencyId=pAgencyId;
    	agencyName=pAgencyName;
    	agencyUrl=pAgencyUrl;
    	agencyTimezone=pAgencyTimezone;
    	agencyLang=pAgencyLang;
    }
    
    public String getAgencyId() {
		return agencyId;
	}

	public String getAgencyName() {
		return agencyName;
	}

	public String getAgencyUrl() {
		return agencyUrl;
	}

	public String getAgencyTimezone() {
		return agencyTimezone;
	}

	public String getAgencyLang() {
		return agencyLang;
	}
    @Override
	public int compareTo(VOAgency arg1)
    {
    	
		// TODO Auto-generated method stub
		return this.agencyId.hashCode() - arg1.getAgencyId().hashCode();
	}
}

	


