package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.vo.ServicioVO;
import model.vo.VOBusUpdate;
import model.vo.VOStop;
import api.ISTSManager;




public class STSManager implements ISTSManager
{
	private Queue<VOBusUpdate> cola = new Queue<VOBusUpdate>();
	private Queue<ServicioVO> servicios = new Queue<ServicioVO>();
	private DoubleLinkedList<VOStop> stops=new DoubleLinkedList<VOStop>();

	@Override
	public void readBusUpdate(File rtFile)
	{

		JSONParser parser = new JSONParser();
		try
		{
			JSONArray jarray = (JSONArray) parser.parse(new FileReader(rtFile.getAbsolutePath()));
			for(Object o: jarray)
			{
				JSONObject bus = (JSONObject) o;

				String numero = (String) bus.get("VehicleNo");
				long tripId = (long) bus.get("TripId");
				String routeNo = (String) bus.get("RouteNo");
				String direccion = (String) bus.get("Direction");
				String destination = (String) bus.get("Destination");
				String pattern = (String) bus.get("Pattern");
				double latitude = (double) bus.get("Latitude");
				double longitude = (double) bus.get("Longitude");
				String time = (String) bus.get("RecordedTime");
				String routeMap = (String) ((JSONObject) bus.get("RouteMap")).get("Href");
				VOBusUpdate update = new VOBusUpdate(numero, tripId, routeNo, direccion, destination, pattern, latitude, longitude, time, routeMap);
				//System.out.println(numero + "==========" +tripId);
				cola.enqueue(update);
			}	
			//System.out.println(cola.size());

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
		// TODO Auto-generated method stub
		final int R = 6371*1000; // Radious of the earth

		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;

		return distance;

	}

	private Double toRad(Double value) {
		return value * Math.PI / 180;
	}




	// TODO Auto-generated method stub


	@Override
	public IStack<VOStop> listStops(Integer tripID) 
	{
		boolean encontro = false;
		model.data_structures.Stack<VOStop> retornar = new model.data_structures.Stack<VOStop>();
		File f = new File("data/RealTime-8-21-BUSES_SERVICE");
		File[] updateFiles = f.listFiles();
		for (int i = 0; i < updateFiles.length; i++) 
		{
		readBusUpdate(updateFiles[i]);
		}
		loadStops();

		for(int i = 0; i<cola.size() && !encontro; i++)
		{
			VOBusUpdate bus = cola.dequeue();
			long IDBus = bus.getTripId();
			if(IDBus == tripID)
			{
				//System.out.println("entro al anterior al primero");
				encontro = true;
				double latitud1 = bus.getLatitude();
				double longitud1 = bus.getLongitude();

				for(int j = 0; j<stops.size(); j++)
				{
					double latitud2 = Double.parseDouble(stops.get(j).darLatitud());
					double longitud2 = Double.parseDouble(stops.get(j).darLongitud());
					double diferencia = getDistance(latitud1, longitud1 , latitud2, longitud2);
					//System.out.println("Entro al primero");
					if(diferencia <= 70)
					{
						retornar.push(stops.get(j));
						//System.out.println("Entro al 2");
						//System.out.println(stops.get(j).darName());
					}

				}
			}

		}

		// TODO Auto-generated method stub
		//System.out.println(retornar.size());
		return retornar;
	}

	@Override
	public void loadStops() 
	{
		String csvFile = "data/stops.txt";
		BufferedReader br = null;
		String line = "";
		//Se define separador ","
		String cvsSplitBy = ",";

		try {
			br = new BufferedReader(new FileReader(csvFile));
			line = br.readLine();
			line = br.readLine();
			while (line  != null) 
			{                
				String[] datos = line.split(cvsSplitBy);
				//Imprime datos.

				VOStop v = new VOStop(datos[0] ,  datos[1], datos[2],  datos[3] , datos[4] , datos[5],  datos[6],  datos[7] ,  datos[8], "");
				//System.out.println(datos[0] + ", " + datos[1] + ", " + datos[2] + ", " + datos[3] + ", " + datos[4] + ", " + datos[5]+ ", " + datos[6]+ ", " + datos[7]+ ", " + datos[8]+ ", " + "");
				stops.add(v);
				line =br.readLine();

			}
			br.close();
			//System.out.println("" +stops.size());
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			if (br != null) 
			{
				try 
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		// TODO Auto-generated method stub

	}

	// TODO Auto-generated method stub



	public void loadServices( File rtFile)
	{
		JSONParser parser = new JSONParser();
		try
		{
			JSONArray jarray = (JSONArray) parser.parse(new FileReader(rtFile.getAbsolutePath()));
			for(Object o: jarray)
			{
				JSONObject bus = (JSONObject) o;

				String numero = (String) bus.get("VehichleNo");
				long tripId = (long) bus.get("TripId");
				String routeNo = (String) bus.get("RouteNo");
				String direccion = (String) bus.get("Direction");
				String destination = (String) bus.get("Destination");
				String pattern = (String) bus.get("Pattern");
				double latitude = (double) bus.get("Latitude");
				double longitude = (double) bus.get("Longitude");
				String time = (String) bus.get("RecordedTime");
				String routeMap = (String) ((JSONObject) bus.get("RouteMap")).get("Href");
				ServicioVO servicio = new ServicioVO(numero, tripId, routeNo, direccion, destination, pattern, latitude, longitude, time, routeMap);
				servicios.enqueue(servicio);
			}
		}
		catch (Exception e)
		{
			System.out.println("Error");
		}
	}



	@Override
	public void ITScargarGTFS() {
		// TODO Auto-generated method stub

	}



	@Override
	public DoubleLinkedList ITSrutasPorEmpresa(String nombreEmpresa, String fecha) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public DoubleLinkedList ITSviajesRestrasadosRuta(String idRuta, String fecha) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public DoubleLinkedList ITSparadasRestrasadasFecha(String fecha) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public DoubleLinkedList ITStransbordosRuta(String idRuta) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public DoubleLinkedList ITSrutasPlanUtilizacion(String fecha, String horaInicio, String horaFin) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public DoubleLinkedList ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public DoubleLinkedList ITSviajesRetrasoTotalRuta(String idRuta, String fecha) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public DoubleLinkedList ITSrestardoHoraRuta(String idRuta, String fecha) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public DoubleLinkedList ITSbuscarViajesParadas(String idOrigen, String idDestino, String fecha, String hortaInicio,
			String horaFin) {
		// TODO Auto-generated method stub
		return null;
	}



	//	@Override
	//	public RouteVo ITSrutaMenorRetardo(String idRuta, String fecha) {
	//		// TODO Auto-generated method stub
	//		return null;
	//	}



	@Override
	public void ITSInit() {
		// TODO Auto-generated method stub

	}



	@Override
	public DoubleLinkedList ITSserviciosMayorDistancia(String fecha) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public DoubleLinkedList ITSretardosViaje(String fecha, String idViaje) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public DoubleLinkedList ITSparadasCompartidas(String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

}
