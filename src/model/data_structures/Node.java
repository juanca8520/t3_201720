package model.data_structures;

public class Node <E>
{
	
	protected E element;
	
	protected Node<E> anterior;

	protected Node<E> siguiente;
	
	public Node (E nElement)
	{
		element = nElement;
		anterior = null;
		siguiente = null;
	}
	
	public void cambiarSiguiente(Node<E> nSiguiente)
	{
		siguiente = nSiguiente;
	}
	
	public void cambiarAnterior(Node<E> nAnterior)
	{
		anterior = nAnterior;
	}
	
	public E darElemento()
	{
		return element;
	}
	
	public void cambiarElemento(E nElemento)
	{
		element = nElemento;
	}
	
//	public String darIdentificador()
//	{
//		return element.darIdentificador();
//	}
	
	public Node<E> darSiguiente()
	{
		return siguiente;
	}
	
	public Node<E> darAnterior()
	{
		return anterior;
	}
}
