package model.data_structures;

import java.util.Iterator;

import java.util.NoSuchElementException;

public class Queue<E> implements IQueue<E>
{

	//Atributos

	private Node<E> primero;

	private Node<E> ultimo;

	private int numeroElementos;


	// Preguntar 
	//	private static class Node<T>
	//	{
	//		private T item;
	//		private Node<T> next;
	//	}


	// Constructores

	public Queue() 
	{
		primero = null;
		ultimo = null;
		numeroElementos = 0;
	}

	//Metodos

	public boolean isEmpty()
	{
		if(ultimo == null)
		{
			return true;
		}
		else {
			return false;
		}
	}

	public int size()
	{
		return numeroElementos;
	}

	public E peek()
	{
		if(isEmpty())
		{
			throw new NoSuchElementException("Queue underflow");
		}
		else {
			return primero.darElemento();
		}
	}

	public void enqueue (E elemento)
	{
		Node<E> agregar = new Node<E>(elemento);
		if(isEmpty())
		{
			primero = agregar;
			ultimo = primero;
		}
		else {
			agregar.cambiarSiguiente(ultimo);
			ultimo.cambiarAnterior(agregar);
			ultimo = agregar;
			
//			ultimo.cambiarSiguiente(agregar);
//			agregar.cambiarAnterior(ultimo);
//			ultimo = agregar;
		}
		numeroElementos++;
	}

	public E dequeue()
	{
		E eliminar = null;
		if(isEmpty())
		{
			throw new NoSuchElementException("Queue underflow");
		}
		
		else{

			eliminar = primero.darElemento();
			primero = primero.darAnterior();
			primero.cambiarSiguiente(null);
			numeroElementos--;
		}
		return eliminar;
	}


//	public String toString ()
//	{
//		StringBuilder s = new StringBuilder();
//		for(T element : this)
//		{
//			s.append(element);
//			s.append(' ');
//		}
//		return s.toString();
//	}
	
	private class ListIterator<E> implements Iterator<E>
	{
		private Node<E> current;
		
		public ListIterator( Node<E> first )
		{
			current = first;
			// TODO Auto-generated constructor stub
		}
		

		@Override
		public boolean hasNext() 
		{
			return current != null;
			// TODO Auto-generated method stub
		}

		@Override
		public E next() 
		{
			if(!hasNext())
			{
				throw new NoSuchElementException();
			}
			E element = current.element;
			current = current.siguiente;
			return element;
			// TODO Auto-generated method stub
		}
		
	}

	
	public Iterator<E> iterator() {
		
		// TODO Auto-generated method stub
		return new ListIterator<>(primero);
	}

}
