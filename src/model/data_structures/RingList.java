package model.data_structures;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class RingList <T extends Comparador<T>> implements Serializable, List<T>
{
    private static final long serialVersionUID = 1L;

	private Node<T> primero;

	private Node<T> ultimo;

	private Node<T> actual;

	public RingList()
	{
		primero = null;
		ultimo = primero;
		actual = primero;
		// TODO Auto-generated constructor stub
	}

	public RingList (T nElement)
	{
		if(nElement == null)
		{
			throw new NullPointerException();
		}
		primero = new Node<T>(nElement);
		ultimo = primero;
		actual = primero;
	}

	/**
	 * agregar ordenadamente los elementos
	 */
	
	public boolean addOrdenado(T e)
	{
		boolean agrego = false;
		Node<T> temporal = primero;
		Node<T> agregar = new Node<T>(e);
		if(e == null)
		{
			throw new NullPointerException();
		}
		else if(primero == null)
		{
			primero = agregar;
			ultimo = primero;
			actual = primero;
			agrego = true;
		}
		else {
			while(!agrego)
			{
				if(e.compareTo( temporal.darElemento()) >= 0)
				{
					if(temporal.darSiguiente() == primero)
					{
						agregar.cambiarSiguiente(primero);
						temporal.cambiarSiguiente(agregar);
						primero.cambiarAnterior(agregar);
						agregar.cambiarAnterior(temporal);
						ultimo = agregar;
						agrego = true;
						//						temporal.cambiarSiguiente(agregar);
						//						agregar.cambiarAnterior(temporal);
						//						agregar.cambiarSiguiente(primero);
						//						primero.cambiarAnterior(agregar);
						//						ultimo = agregar;
						//						agrego = true;
					}
					temporal = temporal.darSiguiente();
				}
				else if(temporal == primero && e.compareTo( temporal.darElemento()) < 0)
				{
					agregar.cambiarSiguiente(primero);
					ultimo.cambiarSiguiente(agregar);
					primero.cambiarAnterior(agregar);
					agregar.cambiarAnterior(ultimo);
					primero = agregar;
					agrego = true;
				}
				else if(size() == 1 && e.compareTo(temporal.darElemento()) < 0)
				{
					agregar.cambiarSiguiente(primero);
					primero.cambiarAnterior(agregar);
					agregar.cambiarSiguiente(primero);
					primero.cambiarAnterior(agregar);
					ultimo = primero;
					primero = agregar;
					agrego = true;
				}
				else if(e.compareTo(temporal.darElemento()) < 0)
				{
					agregar.cambiarSiguiente(temporal);
					temporal.darAnterior().cambiarSiguiente(agregar);
					agregar.cambiarAnterior(temporal.darAnterior());
					temporal.cambiarAnterior(agregar);
					agrego = true;
				}
			}
		}

		// TODO Auto-generated method stub
		return agrego;
	}

	
	public void add(int index, T element) 
	{
		int cont = 0;
		boolean agrego = false;
		Node<T> agregar = new Node<T>(element);
//		if(index < 0 || index > size())
//		{
//			throw new IndexOutOfBoundsException();
//		}
		if(element == null)
		{
			throw new NullPointerException();
		}
		else if(primero == null)
		{
			primero = new Node<T>(element);
			ultimo = primero;
			primero.cambiarSiguiente(ultimo);
			primero.cambiarAnterior(ultimo);
			ultimo.cambiarSiguiente(primero);
			ultimo.cambiarAnterior(primero);
			agrego = true;
		}
		else {
			while(!agrego)
			{
				Node<T> temporal = primero;
				if(cont == index)
				{
					
					if(temporal == primero)
					{	
						ultimo.cambiarSiguiente(agregar);
						agregar.cambiarSiguiente(temporal);
						agregar.cambiarAnterior(ultimo);
						temporal.cambiarAnterior(agregar);
						primero = agregar;
						agrego = true;
					}
					
					else{
						agregar.cambiarSiguiente(temporal);
						temporal.darAnterior().cambiarSiguiente(agregar);
						agregar.cambiarAnterior(temporal.darAnterior());
						temporal.cambiarAnterior(agregar);
						agrego = true;
					}
				}
				cont++;
				temporal = temporal.darSiguiente();
			}
		}
		// TODO Auto-generated method stub

	}

	public void addAtEnd (T elem)
	{
		Node<T> agregar = new Node<T>(elem);
		boolean agrego = false;
		if(elem == null)
		{
			throw new NullPointerException();
		}
		else if(primero == null)
		{
			primero = agregar;
//			ultimo = agregar;
			primero.cambiarSiguiente(agregar);
			agregar.cambiarSiguiente(primero);
			primero.cambiarAnterior(agregar);
			agregar.cambiarAnterior(primero);
			ultimo = primero;
			agrego = true;
		}
		else {
			Node<T> temporal = primero;
			while(!agrego)
			{
				if(temporal.darSiguiente()==primero)
				{
					agregar.cambiarSiguiente(primero);
					temporal.cambiarSiguiente(agregar);
					agregar.cambiarAnterior(temporal);
					primero.cambiarAnterior(agregar);
					ultimo = agregar;
					agrego = true;
				}
				temporal = temporal.darSiguiente();
			}
		}
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() 
	{
		primero = null;
		// TODO Auto-generated method stub

	}

	
	public boolean contiene(T o) 
	{
		Node<T> temporal = primero;
		boolean esta = false;
		while(temporal != ultimo && !esta)
		{
			if(temporal.darElemento() == o)
			{
				esta = true;
			}
			temporal = temporal.darSiguiente();
		}
		// TODO Auto-generated method stub
		return esta;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public T get(int index)
	{
		int cont = 0;
		boolean esta = false;
		T retornar = null;
		Node<T> temporal = primero;
		if(cont < 0 || cont > size() )
		{
			throw new IndexOutOfBoundsException();
		}
		else {
			while(temporal.darSiguiente() != primero)
			{
				if(cont == index)
				{
					retornar = temporal.darElemento();
				}
				temporal = temporal.darSiguiente();
				cont++;
			}
		}
		// TODO Auto-generated method stub
		return retornar;
	}

	@Override
	public int indexOf(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isEmpty()
	{
		if(primero == null)
		{
			return true;
		}
		else {
			return false;
		}
		// TODO Auto-generated method stub
	}

	private class Iterator<T> implements ListIterator<T>
	{
		private Node<T> actual;

		public Iterator(Node<T> nActual)
		{
			actual = nActual;
		}


		public boolean hasNext()
		{
			if(actual.darSiguiente()!=null)
			{
				return true;
			}
			else {
				return false;
			}
		}

		public boolean hasPrevious()
		{
			if(actual.darAnterior()!=null)
			{
				return true;
			}
			else {
				return false;
			}
		}

		public T next()
		{
			if(actual.darSiguiente()==null)
			{
				throw new IndexOutOfBoundsException();
			}
			else{
				return actual.darSiguiente().darElemento();

			}
		}

		public T previous()
		{
			if(actual.darAnterior() == null)
			{
				throw new IndexOutOfBoundsException();
			}
			else {
				return actual.darAnterior().darElemento();

			}
		}


		@Override
		public void add(T e)
		{
			// TODO Auto-generated method stub

		}


		@Override
		public int nextIndex() {
			// TODO Auto-generated method stub
			return 0;
		}


		@Override
		public int previousIndex() {
			// TODO Auto-generated method stub
			return 0;
		}


		@Override
		public void remove() {
			// TODO Auto-generated method stub

		}


		@Override
		public void set(T e) {
			// TODO Auto-generated method stub

		}

	}

	@Override
	public Iterator<T> iterator()
	{

		// TODO Auto-generated method stub
		return new Iterator<>(actual);
	}

	@Override
	public int lastIndexOf(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ListIterator<T> listIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean removeObject(T o)
	{
		Node<T> temporal = primero;
		boolean elimino = false; 
		if (!contiene(o))
		{
			return false;
		}
		else {
			while(!elimino)
			{
				if(temporal.darElemento() == o)
				{
					if(temporal == primero)
					{
						ultimo.cambiarSiguiente(temporal.darSiguiente());
						temporal.darSiguiente().cambiarAnterior(ultimo);
						primero = temporal.darSiguiente();
						elimino = true;
					}
					else if(temporal == ultimo)
					{
						ultimo.darAnterior().cambiarSiguiente(primero);
						primero.cambiarAnterior(ultimo.darAnterior());
						ultimo = temporal.darAnterior();
						elimino = true;
					}
					else {
						temporal.darAnterior().cambiarSiguiente(temporal.darSiguiente());
						temporal.darSiguiente().cambiarAnterior(temporal.darAnterior());
						elimino = true;
					}
				}
				temporal = temporal.darSiguiente();
			}
		}
		return elimino;
		// TODO Auto-generated method stub
	}

	@Override
	public T remove(int index)
	{
		T eliminar = null;
		int cont = 0;
		Node<T> temporal = primero;
		boolean elimino = false;
		if(index < 0 || index > size())
		{
			throw new IndexOutOfBoundsException();
		}
		else {
			while(!elimino)
			{
				if(cont == index)
				{
					if(size() == 1)
					{
						eliminar = primero.darElemento();
						primero = null;
						elimino = true;
					}
					else if(temporal.darSiguiente() == primero)
					{
						eliminar = ultimo.darElemento();
						temporal.darAnterior().cambiarSiguiente(primero);
						primero.cambiarAnterior(temporal.darAnterior());
						ultimo = temporal.darAnterior();
						elimino = true;
					}
					else {
						eliminar = temporal.darElemento();
						temporal.darAnterior().cambiarSiguiente(temporal.darSiguiente());
						temporal.darSiguiente().cambiarAnterior(temporal.darAnterior());
						elimino = true;
					}
				}
				temporal = temporal.darSiguiente();
				cont++;
			}
		}
		// TODO Auto-generated method stub
		return eliminar;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public T set(int index, T element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int size()
	{
		Node<T> temporal = primero;
		int cont = 0;
		while (temporal != ultimo)
		{
			cont++;
			temporal = temporal.darSiguiente();
		}
		
		// TODO Auto-generated method stub
		return cont;
	}

	@Override
	public List<T> subList(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean add(T e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}


//	public int compare(T o1, T o2) 
//	{
//		int diferencia = o1.darIdentificador().compareTo(o2.darIdentificador());
//		if(diferencia > 0)
//		{
//			return 1;
//		}
//		else if(diferencia < 0)
//		{
//			return -1;
//		}
//		else {
//			return 0;
//		}
//		// TODO Auto-generated method stub
//	}

}
