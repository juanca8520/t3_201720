package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Stack <T> implements Iterable<T>, IStack<T>
{

	protected Node<T> primero;
	
	protected Node<T> ultimo;
	
	protected int num;
	
	public Stack() 
	{
		primero = null;
		ultimo = null;
		num = 0;
		// TODO Auto-generated constructor stub
	}
	
	public boolean isEmpty()
	{
		if(primero == null)
		{
			return true;
		}
		else {
			return false;
		}
	}
	
	public int size()
	{
		return num;
	}
	
	public void push (T elem)
	{
		Node<T> agregar = new Node<T>(elem);
		if(isEmpty())
		{
			primero = agregar;
			ultimo = primero;
		}
		else {
			agregar.cambiarSiguiente(ultimo);
			ultimo.cambiarAnterior(agregar);
			ultimo = agregar;
		}
		num++;
//		agregar.cambiarSiguiente(primero);
//		primero.cambiarAnterior(agregar);
//		primero = agregar;
//		num++;
	}
	
	public T pop()
	{
		T elem = null;
		if(!isEmpty())
		{
			elem = ultimo.darElemento();
			ultimo = ultimo.darSiguiente();
			ultimo.cambiarAnterior(null);
			num--;
		}
		else {
			throw new NoSuchElementException();
		}
		return elem;
	}
	
	private class ListIterator<T> implements Iterator<T>
	{

		private Node<T> current;
		
		public ListIterator( Node<T> first ) 
		{
			current = first;
			// TODO Auto-generated constructor stub
		}
	
		@Override
		public boolean hasNext()
		{
			return current.darSiguiente() != null;
			// TODO Auto-generated method stub
		}
		
		public boolean hasPrevious()
		{
			return current.darAnterior() != null;
		}

		@Override
		public T next() {
			// TODO Auto-generated method stub
			return current.darSiguiente().darElemento();
		}
		
		public T previous()
		{
			return current.darAnterior().darElemento();
		}
		
	}
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new ListIterator<>(primero);
	}
	
	public T peek()
	{
		if(isEmpty())
		{
			throw new NoSuchElementException();
		}
		else {
			return primero.darElemento();
		}
	}
	
	public boolean contains(Object elem)
	{
		boolean encontro = false;
		for(int i=0; i<size() && !encontro; i++)
		{
			T elemento = pop();
			if(elemento == elem)
			{
				encontro = true;
			}
		}
		return encontro;
	}

}
