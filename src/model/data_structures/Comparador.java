package model.data_structures;

public interface Comparador<T> {
	
	public int compareTo(T arg1);

}
