package model.data_structures;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.naming.NameAlreadyBoundException;

public class DoubleLinkedList <T extends Comparador<T>> implements Serializable, List<T>
{
	private static 
	final long serialVersionUID = 1L;

	protected Node<T> primero;

	protected Node<T> actual;

	protected Node<T> anterior;

	public DoubleLinkedList()
	{
		primero = null;
		actual = null;
		anterior = null;
	}

	public DoubleLinkedList( T nPrimero )
	{
		if(nPrimero == null)
		{
			throw new NullPointerException();
		}
		primero = new Node<T>(nPrimero);
	}

	
	public boolean add ( T elem ) 
	{
		Node<T> agregar = new Node<T>(elem);
		boolean agrego = false;
		if(elem == null)
		{
			throw new NullPointerException();
		}
		else if ( primero == null )
		{
			primero = agregar;
			agrego = true;
		}
		else{
			
			Node<T> temporal = primero;
			while( !agrego && temporal != null )
			{
				if ( elem.compareTo(temporal.darElemento()) > 0 && temporal.darSiguiente() != null )
				{
					temporal = temporal.darSiguiente();
				}
				else if(size() == 1 && elem.compareTo(temporal.darElemento())<=0 )
				{
					agregar.cambiarSiguiente(primero);
					primero.cambiarAnterior(agregar);
					primero = agregar;
					agrego = true;
				}
				else if( elem.compareTo(temporal.darElemento()) <= 0)
				{
					agregar.cambiarSiguiente(temporal.darSiguiente());
					temporal.cambiarSiguiente(agregar);
					temporal.darSiguiente().cambiarAnterior(agregar);
					agregar.cambiarAnterior(temporal);
					agrego = true;
				}
				else if(temporal.darSiguiente()==null)
				{
					temporal.cambiarSiguiente(agregar);
					agregar.cambiarAnterior(temporal);
					agrego = true;
				}
			}
		}
		return agrego;
	}

	//	@Override
	//	public int compare(T arg0, T arg1) 
	//	{
	//		int diferencia = arg0.darIdentificador().compareTo(arg1.darIdentificador());
	//		if(diferencia>0)
	//		{
	//			return 1;
	//		}
	//		else if(diferencia<0)
	//		{
	//			return -1;
	//		}
	//		else {
	//			return 0;
	//		}
	//		// TODO Auto-generated method stub
	//	}

	@Override
	public void add(int arg0, T arg1) 
	{
		int cont = 0;
		Node<T> agregar = new Node<T>(arg1);
		boolean agrego = false;
		if(arg1 == null)
		{
			throw new NullPointerException();
		}
		else if(primero == null)
		{
			primero = agregar;
		}
		else {
			Node<T> temporal = primero;
			while( !agrego )
			{
				if(cont == arg0)
				{
					if(size()==1)
					{
						agregar.cambiarSiguiente(primero);
						primero.cambiarAnterior(agregar);
						primero = agregar;
						agrego = true;
					}
					else {
						agregar.cambiarSiguiente(temporal);
						temporal.darAnterior().cambiarSiguiente(agregar);
						temporal.cambiarAnterior(agregar);
						agregar.cambiarAnterior(temporal.darAnterior());
						agrego = true;
					}
				}
				cont++;
				temporal = temporal.darSiguiente();

			}
		}
		// TODO Auto-generated method stub

	}

	public void addAtEnd (T element)
	{
		Node<T> agregar = new Node<T>(element);
		boolean agrego = false;
		if(element == null)
		{
			throw new NullPointerException();
		}
		else if(primero == null)
		{
			primero = new Node<T>(element);
		}
		else {
			Node<T> temporal = primero;
			while ( !agrego )
			{
				if(temporal.darSiguiente() == null)
				{
					temporal.cambiarSiguiente(agregar);
					agregar.cambiarAnterior(temporal);
					agrego = true;
				}
				temporal = temporal.darSiguiente();
			}
		}
	}

	@Override
	public boolean addAll(Collection<? extends T> arg0)
	{

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(int arg0, Collection<? extends T> arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() 
	{
		primero = null;
		// TODO Auto-generated method stub

	}

	@Override
	public boolean contains(Object arg0)
	{
		boolean esta = false;
		Node<T> temporal = primero;
		while(temporal.darSiguiente()!=null && !esta)
		{
			if(temporal.darElemento().equals(arg0))
			{
				esta = true;
			}
			temporal = temporal.darSiguiente();
		}
		// TODO Auto-generated method stub
		return esta;
	}

	@Override
	public boolean containsAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public T get(int arg0) 
	{
		T buscado = null;
		int cont = 0;
		boolean encontro = false;
		Node<T> temporal = primero;
		if( arg0 > size() || arg0 < 0 )
		{
			throw new IndexOutOfBoundsException();
		}

		else {
			while( temporal !=null && !encontro )
			{
				if(cont == arg0)
				{
					buscado = temporal.darElemento();
					encontro = true;
				}
				else {
					cont++;
					temporal = temporal.darSiguiente();
				}
			}
		}
		// TODO Auto-generated method stub
		return buscado;
	}

	@Override
	public int indexOf(Object arg0) 
	{
		int cont = 0;
		int pos = -1;
		boolean encontro = false;
		Node<T> temporal = primero;
		while ( temporal != null && !encontro)
		{
			if(temporal==arg0)
			{
				pos = cont;
				encontro = true;
			}
			cont++;
			temporal = temporal.darSiguiente();
		}
		// TODO Auto-generated method stub
		return pos;
	}

	@Override
	public boolean isEmpty()
	{
		if(primero == null)
		{
			return true;
		}
		else {
			return false;
		}
		// TODO Auto-generated method stub

	}

	private class Iterator<T extends Comparador<T>> implements ListIterator<T>
	{
		private Node<T> actual;

		public Iterator(Node<T> nActual)
		{
			actual = nActual;
		}


		public boolean hasNext()
		{
			if(actual.darSiguiente()!=null)
			{
				return true;
			}
			else {
				return false;
			}
		}

		public boolean hasPrevious()
		{
			if(actual.darAnterior()!=null)
			{
				return true;
			}
			else {
				return false;
			}
		}

		public T next()
		{
			if(actual.darSiguiente()==null)
			{
				throw new IndexOutOfBoundsException();
			}
			else{
				return actual.darSiguiente().darElemento();

			}
		}

		public T previous()
		{
			if(actual.darAnterior() == null)
			{
				throw new IndexOutOfBoundsException();
			}
			else {
				return actual.darAnterior().darElemento();

			}
		}


		@Override
		public void add(T e)
		{

			// TODO Auto-generated method stub

		}


		@Override
		public int nextIndex() {
			// TODO Auto-generated method stub
			return 0;
		}


		@Override
		public int previousIndex() {
			// TODO Auto-generated method stub
			return 0;
		}


		@Override
		public void remove() {
			// TODO Auto-generated method stub

		}


		@Override
		public void set(T e) {
			// TODO Auto-generated method stub

		}

	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>(actual);
	}

	@Override
	public int lastIndexOf(Object arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ListIterator<T> listIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListIterator<T> listIterator(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean removeObject(Object arg0)
	{
		Node<T> temporal = primero;
		boolean elimino = false;
		while (temporal != null && !elimino) 
		{
			if(size() == 1)
			{
				primero = null;
				elimino = true;
			}
			else if(temporal.darElemento().equals(arg0))
			{
				if(temporal == primero)
				{
					primero = primero.darSiguiente();
					elimino = true;
				}
				else if(temporal.darSiguiente()==null)
				{
					temporal.darAnterior().cambiarSiguiente(temporal.darSiguiente());
					elimino = true;
				}
				else {
					temporal.darAnterior().cambiarSiguiente(temporal.darSiguiente());
					temporal.darSiguiente().cambiarAnterior(temporal.darAnterior());
					elimino = true;
				}
			}
			temporal = temporal.darSiguiente();
		}
		// TODO Auto-generated method stub
		return elimino;
	}

	public T removeAtK(int arg0) 
	{
		int cont = 0;
		boolean elimino = false;
		T eliminado = null;
		Node<T> temporal = primero;
		while (temporal != null)
		{
			if(arg0 == 0)
			{
				eliminado = primero.darElemento();
				primero = primero.darSiguiente();
				elimino = true;
			}
			else if(cont == arg0 && temporal.darSiguiente()==null)
			{
				eliminado = temporal.darElemento();
				temporal.darAnterior().cambiarSiguiente(temporal.darSiguiente());
				elimino = true;
			}
			else if(cont == arg0)
			{
				eliminado = temporal.darElemento();
				temporal.darAnterior().cambiarSiguiente(temporal.darSiguiente());
				temporal.darSiguiente().cambiarAnterior(temporal.darAnterior());
				elimino = true;
			}
			temporal = temporal.darSiguiente();
			cont++;
		}
		// TODO Auto-generated method stub
		return eliminado;
	}

	@Override
	public boolean removeAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public T set(int arg0, T arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int size() 
	{
		int cont = 0;
		Node<T> temporal = primero;

		while (temporal != null)
		{
			cont++;	
			temporal = temporal.darSiguiente();

		}


		// TODO Auto-generated method stub
		return cont;
	}

	@Override
	public List<T> subList(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	

	@Override
	public T remove(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}


}
