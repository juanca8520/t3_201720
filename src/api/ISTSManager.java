package api;

import java.io.File;


import model.data_structures.Comparador;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IStack;
import model.vo.VOStop;

public interface ISTSManager<T extends Comparador<T>> {

	/**
	 * Reads a file in json format containing bus updates in real time
	 * @param rtFile - The file to read
	 */
	public void readBusUpdate(File rtFile);
	
	public IStack<VOStop> listStops (Integer tripID);

	public void loadStops();
	
	public void ITScargarGTFS();

	DoubleLinkedList<T> ITSrutasPorEmpresa(String nombreEmpresa, String fecha);
	
	DoubleLinkedList<T> ITSviajesRestrasadosRuta(String idRuta, String fecha);
	
	DoubleLinkedList<T> ITSparadasRestrasadasFecha(String fecha);
	
	DoubleLinkedList<T> ITStransbordosRuta(String idRuta);
	
	DoubleLinkedList<T> ITSrutasPlanUtilizacion(String fecha, String horaInicio, String horaFin);
	
	DoubleLinkedList<T> ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha);
	
	DoubleLinkedList<T> ITSviajesRetrasoTotalRuta(String idRuta, String fecha);
	
	DoubleLinkedList<T> ITSrestardoHoraRuta(String idRuta, String fecha);
	
	DoubleLinkedList<T> ITSbuscarViajesParadas(String idOrigen, String idDestino, String fecha, String hortaInicio, String horaFin);
	
//	RouteVo ITSrutaMenorRetardo (String idRuta, String fecha);
	
	void ITSInit();
	
	DoubleLinkedList<T> ITSserviciosMayorDistancia(String fecha);
	
	DoubleLinkedList<T> ITSretardosViaje(String fecha, String idViaje);
	
	DoubleLinkedList<T> ITSparadasCompartidas(String fecha);
	
	
}
