package estructuras.test;

import junit.framework.TestCase;
import model.data_structures.Stack;

public class StackTest <E> extends TestCase
{
	
	private Stack stack;
	
	public void setUp()
	{
		stack = new Stack<>();
	}
	
	public void testPush()
	{
		setUp();
		
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);
		
		assertEquals(false, stack.isEmpty());
		assertEquals("El numero de objetos no es el que deberia ser", 5, stack.size());	
	}
	
	public void testPop()
	{
		setUp();
		
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);
		assertEquals("El elemento eliminado no es el que deberia ser", 5, stack.pop());
		assertEquals("El tamanho no es el que seberia ser", 4, stack.size());
		stack.pop();
		assertEquals("El tamanho no es el que seberia ser", 3, stack.size());

		
	}
	
	public void testPeek()
	{
		setUp();
		
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);
		
		assertEquals("El elemento no es el que deberia", 1, stack.peek());
		setUp();
		
		stack.push(3);
		
		assertEquals("El elemento no es el que deberia", 3, stack.peek());
	}
	
	public void testContains()
	{
setUp();
		
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);
		
		System.out.println(stack.size());
		
		assertEquals(false, stack.contains(7));
		assertEquals(true, stack.contains(2));
	}
}
