package estructuras.test;

import junit.framework.TestCase;

import model.data_structures.Queue;

public class QueueTest<E> extends TestCase
{
	
	private Queue queue;
	
	public void setUp()
	{
		queue = new Queue<>();
	}
	
	public void testEnqueue()
	{
		setUp();
		
		queue.enqueue(1);
		queue.enqueue(2);
		queue.enqueue(3);
		queue.enqueue(4);
		queue.enqueue(5);
		assertEquals(false,queue.isEmpty());
		assertEquals(5, queue.size());
	}
	
	public void testDequeue()
	{
		setUp();
		queue.enqueue(1);
		queue.enqueue(2);
		queue.enqueue(3);
		queue.enqueue(4);
		queue.enqueue(5);
		
		assertEquals("No se elimina el esperado", 1, queue.dequeue());
		assertEquals("No se elimina el esperado", 2, queue.dequeue());
		assertEquals("No se tiene el tamanho de lista correcto", 3, queue.size());
	}
	
	public void testPeek()
	{
		setUp();
		queue.enqueue(1);
		queue.enqueue(2);
		queue.enqueue(3);
		queue.enqueue(4);
		queue.enqueue(5);
		
		assertEquals("No se retorna el esperado", 1, queue.peek());
		
	}
	

}
