package estructuras.test;

import junit.framework.TestCase;
import model.data_structures.Comparador;
import model.data_structures.RingList;
import model.vo.VOAgency;

public class RingListTest<T extends Comparador<T>> extends TestCase 
{

	private RingList lista;
	
	private VOAgency primero;
	private VOAgency segundo;
	private VOAgency tercero;
	private VOAgency cuarto;
	
	
	public void setUp()
	{
		lista = new RingList<>();
		
		primero = new VOAgency("Uno", "Prueba 1", "https", "zona 1", "no se");
		segundo = new VOAgency("Dos", "Prueba 2", "https1", "zona 2", "no se 1");
		tercero = new VOAgency("Tres", "Prueba 3", "https", "zona 3", "no se 2");
		cuarto = new VOAgency("Cuarto", "Prueba 4", "https", "zona 4", "no se 3");
	}

	public void testAddIntT()
	{
		setUp();

		lista.addOrdenado(primero);
		lista.addOrdenado(segundo);
		lista.addOrdenado(tercero);
		lista.addOrdenado(cuarto);

		for(int i=0; i<lista.size() ; i++)
		{
			System.out.println(lista.get(i));
		}
		assertEquals("El tamanho de la lista no es el correcto", 3, lista.size());

	}

	public void testAddAtEnd()
	{
		setUp();

		lista.addAtEnd(primero);
		lista.addAtEnd(segundo);
		lista.addAtEnd(tercero);
		lista.addAtEnd(cuarto);	
		
		assertEquals("El orden no es el correcto", primero, lista.get(0));
		assertEquals("El orden no es el correcto", segundo, lista.get(1));


	}

	public void testClear() 
	{
		setUp();

		lista.addAtEnd(new VOAgency("Uno", "Prueba 1", "https", "zona 1", "no se"));
		lista.addAtEnd(new VOAgency("Dos", "Prueba 2", "https2", "zona 2", "no se 1"));
		lista.addAtEnd(new VOAgency("Tres", "Prueba 3", "https3", "zona 3", "no se 2"));
		lista.addAtEnd(new VOAgency("Cuatro", "Prueba 4", "https4", "zona 4", "no se 3"));	
		
		
		lista.clear();
		assertEquals("No se estan eliminando los elementos",true, lista.isEmpty());
	}

	public void testContiene()
	{
		setUp();

		lista.addAtEnd(primero);
		lista.addAtEnd(segundo);
		lista.addAtEnd(tercero);
		lista.addAtEnd(cuarto);	
		
		VOAgency prueba = new VOAgency("", "", "", "", "");
		
		assertEquals("La lista no contiene el elemento esperado", true, lista.contiene(segundo));
		assertEquals("La lista contiene un elemento que no deberia", false, lista.contiene(prueba));
		
		
	}

	public void testGet() 
	{
		setUp();
		lista.addAtEnd(primero);
		lista.addAtEnd(segundo);
		lista.addAtEnd(tercero);
		lista.addAtEnd(cuarto);	
		
		
		assertEquals("La lista no esta retornando el objeto que deberia", tercero , lista.get(2));
	}

	public void testIsEmpty()
	{
		setUp();
		
		lista.addAtEnd(new VOAgency("Uno", "Prueba 1", "https", "zona 1", "no se"));
		lista.addAtEnd(new VOAgency("Dos", "Prueba 2", "https2", "zona 2", "no se 1"));
		lista.addAtEnd(new VOAgency("Tres", "Prueba 3", "https3", "zona 3", "no se 2"));
		lista.addAtEnd(new VOAgency("Cuatro", "Prueba 4", "https4", "zona 4", "no se 3"));	
		
		
		lista.clear();
		
		assertEquals("No se eliminan todos los elementos", true, lista.isEmpty());
	}

	public void testRemoveObject()
	{
		setUp();

		lista.addAtEnd(primero);
		lista.addAtEnd(segundo);
		lista.addAtEnd(tercero);
		lista.addAtEnd(cuarto);	
		
		
		System.out.println(lista.size());
		assertEquals(true,lista.removeObject(tercero));
		System.out.println(lista.size());
		assertEquals("No se elimina el objeto que deberia", 2, lista.size());
		assertEquals(false, lista.contiene(tercero));
		
	}

	public void testRemoveInt() 
	{
		setUp();

		lista.addAtEnd(primero);
		lista.addAtEnd(segundo);		
		lista.addAtEnd(tercero);		
		lista.addAtEnd(cuarto);
		
		lista.remove(3);
		assertEquals("No se elimino el objeto", 2, lista.size()  );	
		assertEquals("El objeto aun existe", false, lista.contains(2));
		}

	public void testSize()
	{
		setUp();

		lista.addAtEnd(new VOAgency("Uno", "Prueba 1", "https", "zona 1", "no se"));
		lista.addAtEnd(new VOAgency("Dos", "Prueba 2", "https2", "zona 2", "no se 1"));
		lista.addAtEnd(new VOAgency("Tres", "Prueba 3", "https3", "zona 3", "no se 2"));
		lista.addAtEnd(new VOAgency("Cuatro", "Prueba 4", "https4", "zona 4", "no se 3"));	
		
		
		assertEquals("El tamanho no es el esperado", 3, lista.size());
	}

}
