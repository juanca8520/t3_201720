package estructuras.test;

import junit.framework.TestCase;
import model.data_structures.DoubleLinkedList;
import model.vo.VOAgency;

public class DoubleLinkedListTest<T> extends TestCase
{
	private DoubleLinkedList<VOAgency> lista;
	
	private VOAgency primero;
	private VOAgency segundo;
	private VOAgency tercero;
	private VOAgency cuarto;

	public void setUp()
	{
		lista = new DoubleLinkedList<VOAgency>();
		
		primero = new VOAgency("Primero", "nombre1", "https1", "Hora1", "x1");
		segundo = new VOAgency("Segundo", "nombre2", "https2", "Hora2", "x2");
		tercero = new VOAgency("Tecero", "nombre3", "https3", "hora3", "x3");
		cuarto = new VOAgency("Cuarto", "nombre3", "https4", "hora4", "x4");
	}

	public void testAddAtEnd()
	{
		setUp();

		lista.add(primero);
		lista.add(segundo);
		lista.add(tercero);
		lista.add(cuarto);

				for(int i = 0; i<lista.size(); i++)
				{
					System.out.println(lista.get(i));
				}

		assertEquals("El tamanho no es el que deberia", 4, lista.size());
		assertEquals(false, lista.isEmpty());
	}

	public void testGet()
	{
		setUp();

		lista.addAtEnd(primero);
		lista.addAtEnd(segundo);
		lista.addAtEnd(tercero);
		lista.addAtEnd(cuarto);
		

		assertEquals("El elemento retornado no es el correcto", tercero, lista.get(2));
		assertEquals("El elemento retornado no es el correcto", segundo, lista.get(1));
		
		setUp();

		lista.addAtEnd(primero);
		lista.addAtEnd(segundo);
		lista.addAtEnd(tercero);
		lista.addAtEnd(cuarto);
		
		
		assertEquals("El elemento recuperado no es el correcto", cuarto, lista.get(3));

	}

	public void testRemove() 
	{
		setUp();

		lista.addAtEnd(primero);
		lista.addAtEnd(segundo);
		lista.addAtEnd(tercero);
		lista.addAtEnd(cuarto);
	

		assertEquals("El elemento eliminado no es el correcto", cuarto, lista.removeAtK(3));
		assertEquals("El tamanho de la lista luego de eliminar no es el correcto", 3, lista.size());
		
		assertEquals("No se elimino el elemento", true, lista.removeObject(tercero));
//		assertEquals("El elemento eliminado no fue el correcto", cuarto, lista.get(2));
		assertEquals("El tamanho no es correcto", 2, lista.size());
		
	}

	public void testSize()
	{
		
		setUp();

		lista.addAtEnd(primero);
		lista.addAtEnd(segundo);
		lista.addAtEnd(tercero);
		lista.addAtEnd(cuarto);
	
		assertEquals("El tamanho no es el correcto", 4, lista.size());

	}
	
	

}
